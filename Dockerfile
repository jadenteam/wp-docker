ARG MAJOR_PHP_VERSION=7.4

FROM wordpress:php${MAJOR_PHP_VERSION}-apache

ARG PHP_OPCACHE_VALIDATE_TIMESTAMPS=1

LABEL maintainer="Aaron HS (asiraky@gmail.com)"

# setup timezone
ENV TZ Australia/Sydney
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
ENV DEBIAN_FRONTEND noninteractive

# configure opcache
COPY opcache.ini "$PHP_INI_DIR/conf.d"

# use the default production configuration
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

COPY configuration.ini "$PHP_INI_DIR/conf.d/z-configuration.ini"
