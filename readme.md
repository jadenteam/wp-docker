## Build

```bash
docker build --build-arg MAJOR_PHP_VERSION=7.4 -t aaronhs/wordpress:php7.4-apache .
```

## Deploy

1) Push the image to dockerhub:
```bash
docker push aaronhs/wordpress:php7.4-apache
```
2) Update the task definition in AWS to point to the new image
3) Force a new deployment on the container in ECS
